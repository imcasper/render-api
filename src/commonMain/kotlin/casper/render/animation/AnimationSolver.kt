package casper.render.animation

import casper.math.clampDouble
import casper.render.model.AnimationPlayMode

object AnimationSolver {
	fun <Custom> create(keys: List<AnimationKey<Custom>>, time: Double, mode: AnimationPlayMode): AnimationState<Custom>? {
		if (keys.isEmpty()) return null
		if (keys.size == 1) return AnimationState(keys[0], keys[0], 0.0)
		val startFrame = keys.first()
		val finishFrame = keys.last()

		if (finishFrame.time < startFrame.time) {
			throw Error("Invalid animation keys: $keys")
		}

		val duration = finishFrame.time - startFrame.time

		var localTime = time - startFrame.time

		if (mode == AnimationPlayMode.CLAMP) {
			localTime = clampDouble(localTime, 0.0, duration)
		}
		if (mode == AnimationPlayMode.WRAP) {
			localTime %= duration
			if (localTime < 0.0) localTime += duration
		}

		if (mode == AnimationPlayMode.MIRROR) {
			val doubleDuration = duration * 2.0
			localTime %= doubleDuration
			if (localTime < 0.0) localTime += doubleDuration

			if (localTime > duration) localTime = doubleDuration - localTime
		}

		if (localTime < 0.0 || localTime > duration) {
			throw Error("Invalid animation localTime. time: $localTime, mode: $mode, keys: $keys")
		}

		val ROUND_ERROR = 0.000001
		for (nextFrameIndex in 1 until keys.size) {
			val prevFrameIndex = nextFrameIndex - 1
			val prevFrame = keys[prevFrameIndex]
			val nextFrame = keys[nextFrameIndex]

			val firstTimeDelta = localTime - (prevFrame.time - startFrame.time)
			val nextTimeDelta = (ROUND_ERROR + nextFrame.time - startFrame.time) - localTime
			if (firstTimeDelta >= 0.0 && nextTimeDelta >= 0.0) {
				val factor = firstTimeDelta / (firstTimeDelta + nextTimeDelta)
				return AnimationState(prevFrame, nextFrame, factor)
			}
		}
		throw Error("Invalid animation resolve. time: $time, mode: $mode, keys: $keys")
	}
}