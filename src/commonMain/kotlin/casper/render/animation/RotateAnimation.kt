package casper.render.animation

import casper.geometry.Quaternion
import casper.geometry.Transform
import casper.geometry.interpolateQuaternion
import casper.render.model.AnimationPlayMode

data class RotateAnimation(override val keys: List<AnimationKey<Quaternion>>) : TransformAnimation<Quaternion> {
	override fun execute(source: Transform, frame: Double, mode: AnimationPlayMode): Transform {
		val state = AnimationSolver.create(keys, frame, mode) ?: return source
		val value = interpolateQuaternion(state.prev.value, state.next.value, state.factor)
		return source.copy(rotation = value)
	}
}