package casper.render.animation

import casper.geometry.Transform
import casper.geometry.Vector3d
import casper.geometry.interpolateVector
import casper.render.model.AnimationPlayMode

data class ScaleAnimation(override val keys: List<AnimationKey<Vector3d>>) : TransformAnimation<Vector3d> {
	override fun execute(source: Transform, frame: Double, mode: AnimationPlayMode): Transform {
		val state = AnimationSolver.create(keys, frame, mode) ?: return source
		val value = interpolateVector(state.prev.value, state.next.value, state.factor)
		return source.copy(scale = value)
	}
}