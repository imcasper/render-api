package casper.render.animation

data class AnimationKey<Custom>(val time: Double, val value: Custom)