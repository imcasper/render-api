package casper.render.animation

class AnimationState<Custom>(val prev: AnimationKey<Custom>, val next: AnimationKey<Custom>, val factor: Double)

