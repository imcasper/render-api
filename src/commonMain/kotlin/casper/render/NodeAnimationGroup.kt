package casper.render

data class NodeAnimationGroup(val name:String, val animations:List<NodeAnimation>)