package casper.render.model

import casper.render.material.MaterialReference
import casper.render.vertex.Vertices
import casper.render.vertex.VerticesReference

class Mesh(
		var vertices: VerticesReference,
		var material: MaterialReference,
		var name: String? = null
) {
	fun copy(vertices: VerticesReference = this.vertices, material: MaterialReference = this.material, name: String? = this.name): Mesh {
		return Mesh(vertices, material, name)
	}

	fun copy(setting: ModelCopySetting = ModelCopySetting()): Mesh {
		return Mesh(
				if (setting.shareVertices) vertices else VerticesReference(vertices.data.slice(vertices.data.indices), vertices.name),
				if (setting.shareMaterials) material else MaterialReference(material.data, material.name),
				name
		)
	}

}


