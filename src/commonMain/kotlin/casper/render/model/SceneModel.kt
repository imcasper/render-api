package casper.render.model

import casper.render.material.Material
import casper.render.material.MaterialReference
import casper.render.vertex.Vertices
import casper.render.vertex.VerticesReference

/**
 *	Объект, описывающий структуру сцены (вершины, дети, родители)
 *	Вы можете добавить объект на сцену несколько раз (элементы будут выглядеть идентично), или делать копии каждый раз при добавлении
 */
data class SceneModel(val mesh: Mesh? = null, val children: Set<SceneNode> = emptySet(), val name: String? = null) {
	constructor(vertices: Vertices, material: Material, name: String? = null) :
			this(Mesh(VerticesReference(vertices), MaterialReference(material)), name = name)

	constructor(vertices: VerticesReference, material: Material, name: String? = null) :
			this(Mesh(vertices, MaterialReference(material)), name = name)

	constructor(vertices: Vertices, material: MaterialReference, name: String? = null) :
			this(Mesh(VerticesReference(vertices), material), name = name)

	constructor(vertices: VerticesReference, material: MaterialReference, name: String? = null) :
			this(Mesh(vertices, material), name = name)

	fun copy(setting: ModelCopySetting = ModelCopySetting()): SceneModel {
		return SceneModel(
				mesh?.copy(setting),
				children.map { it.copy(setting) }.toMutableSet(),
				name
		)
	}
}

