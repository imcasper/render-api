package casper.render.model

import casper.render.animation.TransformAnimation

data class TimeLine(
		val animations: List<TransformAnimation<out Any>> = emptyList(),
		val animationPlayMode: AnimationPlayMode = AnimationPlayMode.WRAP,
		val timeScale: Double = 1.0,
		val timeOffset: Double = 0.0
)