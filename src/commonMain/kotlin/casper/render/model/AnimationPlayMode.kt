package casper.render.model

enum class AnimationPlayMode {
	WRAP,
	CLAMP,
	MIRROR
//	EQUAL,
}