package casper.render.model

import casper.geometry.Transform
import casper.signal.concrete.EmptySignal
import casper.signal.concrete.Signal
import casper.signal.util.NotifyProperty

class SceneNode(transform: Transform = Transform.IDENTITY, model: SceneModel = SceneModel(), timeLine: TimeLine = TimeLine()) {
	val ID = ++lastID
	val changed = EmptySignal()
//	val nextTimeFuture = Signal<Double>()

	var transform by NotifyProperty(transform, changed)
	var model by NotifyProperty(model, changed)
	var timeLine by NotifyProperty(timeLine, changed)


	fun copy(transform: Transform = this.transform, model: SceneModel = this.model, timeLine: TimeLine = this.timeLine): SceneNode {
		return SceneNode(transform, model, timeLine)
	}

	fun addChild(value: SceneNode) {
		if (value == this) {
			throw Error("Can't add himself to children")
		}
		model = model.copy(children = model.children + value)
	}

	fun removeChild(value: SceneNode) {
		if (value == this) {
			throw Error("Can't remove himself to children")
		}
		model = model.copy(children = model.children - value)
	}

	fun copy(setting: ModelCopySetting): SceneNode {
		return SceneNode(
				transform,
				model.copy(setting),
				if (setting.shareAnimation) timeLine else timeLine.copy()
		)
	}

	/**
	 * 	recursive setup animationPlayMode
	 */
	fun setPlayMode(mode: AnimationPlayMode) {
		timeLine = timeLine.copy(animationPlayMode = mode)
		model.children.forEach {
			it.setPlayMode(mode)
		}
	}

	fun setTimeOffset(value:Double) {
		timeLine = timeLine.copy(timeOffset = value)
	}

	fun setPlaySpeed(value:Double) {
		timeLine = timeLine.copy(timeScale = value)
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as SceneNode

		if (ID != other.ID) return false

		return true
	}

	override fun hashCode(): Int {
		return ID
	}

	companion object {
		private var lastID = 0
	}

}