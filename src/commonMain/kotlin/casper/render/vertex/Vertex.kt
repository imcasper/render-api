package casper.render.vertex

import casper.geometry.Vector2d
import casper.geometry.Vector3d
import casper.types.Color4d

data class Vertex(
		val position: Vector3d,
		val normal: Vector3d? = null,
		val color: Color4d? = null,
		val uvAlbedo: Vector2d? = null,
		val uvAmbient: Vector2d? = null,
		val uvBump: Vector2d? = null,
		val uvEmissive: Vector2d? = null,
		val uvMetallic: Vector2d? = null,
		val uvOpacity: Vector2d? = null,
		val uvReflection: Vector2d? = null,
		val uvRoughness: Vector2d? = null
) {
	init {
		if (!position.isFinite()) {
			throw Error("Invalid position: $position")
		}
		if (normal != null && !normal.isFinite()) {
			throw Error("Invalid normal: $normal")
		}
	}
}