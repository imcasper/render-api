package casper.render.extension

import casper.geometry.polygon.Octagon
import casper.geometry.polygon.Quad
import casper.geometry.polygon.Triangle
import casper.render.vertex.Vertex
import casper.render.vertex.Vertices


class VerticesBuilder {

	private val values = mutableListOf<Vertex>()

	fun clear() {
		values.clear()
	}

	fun get(): Vertices {
		return values
	}

	fun add(vertex: Vertex) {
		values.add(vertex)
	}

	fun add(shape: Triangle<Vertex>): Vertices {
		add(shape.v0)
		add(shape.v1)
		add(shape.v2)
		return get()
	}

	fun add(shape: Quad<Vertex>): Vertices {
		add(shape.getFace(0))
		add(shape.getFace(1))
		return get()
	}

	fun add(shape: Octagon<Vertex>): Vertices {
		shape.getFaces().forEach {
			add(it)
		}
		return get()
	}
}
