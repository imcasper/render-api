package casper.render.extension

import casper.geometry.Transform
import casper.render.animation.*
import casper.render.model.SceneNode

/**
 * 	Иногда анимация не меняет трансформацию во времени
 * 	Тогда ее можно применить один раз и удалить из элемента
 */
object AnimationCollapse {

	fun execute(node: SceneNode, animation: TransformAnimation<out Any>): SceneNode {
		if (animation is RotateAnimation) {
			val key = collapseAnimationKey(animation.keys)
			if (key != null) {
				val transform = node.transform.copy(rotation = key.value)
				return removeTransformAnimation(node, animation, transform)
			}
		} else if (animation is TranslateAnimation) {
			val key = collapseAnimationKey(animation.keys)
			if (key != null) {
				val transform = node.transform.copy(position = key.value)
				return removeTransformAnimation(node, animation, transform)
			}
		} else if (animation is ScaleAnimation) {
			val key = collapseAnimationKey(animation.keys)
			if (key != null) {
				val transform = node.transform.copy(scale = key.value)
				return removeTransformAnimation(node, animation, transform)
			}
		}
		return node
	}

	private fun <Custom : Any> collapseAnimationKey(animations: List<AnimationKey<Custom>>): AnimationKey<Custom>? {
		val first = animations.firstOrNull() ?: return null

		animations.forEach {
			if (it.value != first.value) return null
		}
		return first
	}

	private fun removeTransformAnimation(node: SceneNode, animation: TransformAnimation<out Any>, transform: Transform): SceneNode {
		val timeline = node.timeLine.copy(animations = node.timeLine.animations - animation)
		return node.copy(transform = transform, timeLine = timeline)
	}

}