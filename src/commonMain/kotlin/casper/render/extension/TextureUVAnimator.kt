package casper.render.extension

import casper.core.DisposableHolder
import casper.geometry.Matrix4d
import casper.geometry.Vector3d
import casper.render.Render
import casper.render.material.TextureTransform
import casper.signal.util.then

class TextureUVAnimator(render: Render, transform: TextureTransform, var velocity: Vector3d, var position: Vector3d = Vector3d.ZERO) : DisposableHolder() {
	private var time = 0.0

	init {
		render.nextTimeFuture.then(components) {
			time += it
			transform.matrix = Matrix4d.translate(position + velocity * time)
		}
	}

}