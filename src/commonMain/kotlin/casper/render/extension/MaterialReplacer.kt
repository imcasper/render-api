package casper.render.extension

import casper.render.material.MaterialReference
import casper.render.model.SceneModel

object MaterialReplacer {
	fun execute(model: SceneModel, customName: String?, replacer: (MaterialReference) -> MaterialReference?): SceneModel {
		return iterate(model, customName, replacer, mutableMapOf())
	}

	private fun iterate(model: SceneModel, customName: String?, replacer: (MaterialReference) -> MaterialReference?, materialMap: MutableMap<MaterialReference, MaterialReference?>): SceneModel {
		val children = model.children.map {
			it.copy(model = iterate(it.model, null, replacer, materialMap))
		}.toMutableSet()

		val mesh = model.mesh
		val nextName = customName ?: model.name

		if (mesh != null) {
			val last = mesh.material
			val next = materialMap.getOrPut(last) {
				replacer(last)
			}
			if (next != null) {
				return model.copy(name = nextName, children = children, mesh = mesh.copy(material = next))
			}
		}
		return model.copy(name = nextName, children = children)
	}
}