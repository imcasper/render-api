package casper.render.extension

import casper.geometry.Transform
import casper.render.material.Material
import casper.render.material.MaterialReference
import casper.render.model.Mesh
import casper.render.model.SceneModel
import casper.render.model.SceneNode
import casper.render.model.TimeLine
import casper.render.vertex.Vertex
import casper.render.vertex.Vertices
import casper.render.vertex.VerticesReference

data class MergeKey(val material: Material, val timeLine: TimeLine) {
	var materialName: String? = null
	var verticesName: String? = null
}

data class MergeInfo(val key: MergeKey, val mesh: Mesh, val transform: Transform, val name: String?)


object ModelSimplifier {

	fun execute(model: SceneModel): SceneModel {
		model.children.forEach { node ->
		}
		val children = mergeModelTransform(model.children)
		return model.copy(children = children)
	}

	private fun mergeModelTransform(values: Collection<SceneNode>): MutableSet<SceneNode> {
		val materialMap = mutableMapOf<MergeKey, MutableList<MergeInfo>>()
		values.forEach { originalNode ->
			var node = originalNode
			node.timeLine.animations.forEach { animation ->
				node = AnimationCollapse.execute(node, animation)
			}

			val subMesh = node.model.mesh
			if (subMesh != null) {
				val key = MergeKey(subMesh.material.data, node.timeLine)
				key.materialName = subMesh.material.name
				key.verticesName = subMesh.vertices.name

				val subMeshList = materialMap.getOrPut(key) {
					mutableListOf()
				}
				subMeshList += MergeInfo(key, subMesh, node.transform, node.model.name)
			}
		}

		return materialMap.map {
			val key = it.key
			val list = it.value
			if (list.size == 1) {
				val item = list.first()
				SceneNode(model = SceneModel(mesh = item.mesh), transform = item.transform, timeLine = key.timeLine)
			} else {
				val vertices = mergeVertices(list)
				val name = list.firstOrNull()?.name ?: ""
				val mesh = Mesh(VerticesReference(vertices, it.key.verticesName), MaterialReference(it.key.material, it.key.materialName), name)
				SceneNode(model = SceneModel(mesh = mesh), timeLine = key.timeLine)
			}
		}.toMutableSet()
	}

	private fun mergeVertices(values: MutableList<MergeInfo>): Vertices {
		val builder = VerticesBuilder()

		values.forEach {
			val transform = it.transform
			val mesh = it.mesh
			mesh.vertices.data.forEach {
				val normal = it.normal

				val matrix = transform.getMatrix()

				builder.add(it.copy(
						position = matrix.transform(it.position),
						normal = if (normal != null) transform.rotation.transform(normal) else null
				))
			}
		}

		return builder.get()
	}

}