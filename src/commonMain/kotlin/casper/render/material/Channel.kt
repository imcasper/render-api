package casper.render.material

import casper.collection.map.ByteMap2d
import casper.collection.map.IntMap2d
import casper.geometry.Matrix4d
import casper.types.Color4d

/**
 * 	texture filter
 *
 * 	it is equal opengl min-filter
 */
enum class Sampling {
	LINEAR,
	NEAREST
}

data class TextureTransform(
		var sampling: Sampling = Sampling.LINEAR,
		var matrix: Matrix4d? = null
)

/**
 * @param transform -- enable custom texture transformation (offset, rotation, scale etc)
 */
data class TextureReference(
		var data: IntMap2d,
		var name: String? = null,
		var transform: TextureTransform = TextureTransform()
) : ColorReference

data class CubeTextureReference(
		var data: CubeTexture,
		var name: String? = null,
		var transform: TextureTransform = TextureTransform()
) : ColorReference


data class FloatMapReference(
		var data: ByteMap2d,
		var name: String? = null,
		var transform: TextureTransform = TextureTransform()
) : FloatReference

data class CubeTexture(
		val px: IntMap2d,
		val nx: IntMap2d,
		val py: IntMap2d,
		val ny: IntMap2d,
		val pz: IntMap2d,
		val nz: IntMap2d
)


data class ColorConstantReference(var data: Color4d, var name: String? = null) : ColorReference
data class FloatConstantReference(var data: Double, var name: String? = null) : FloatReference


interface ColorReference

interface FloatReference