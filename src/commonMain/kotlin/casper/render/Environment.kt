package casper.render

import casper.geometry.Vector3d
import casper.render.material.CubeTextureReference
import casper.types.Color4d

data class Light(
		val direction: Vector3d,
		val intensity: Double
)

data class Environment(
		val backColor: Color4d,
		val reflectionTexture: CubeTextureReference?,
		val light: Light?,
		val shadow: Boolean
)