package casper.render

import casper.geometry.*
import casper.render.model.SceneNode
import casper.signal.concrete.Future

interface Camera {
	val viewport:Vector2i
	val view:Matrix4d
	val projection:Matrix4d
	val viewProjection:Matrix4d

	var transform:Transform

	fun worldToScreen(worldPosition:Vector3d, clampByViewport:Boolean):Vector2d {
		val relativeScreenPosition = viewProjection.transform(worldPosition)
		val screenSize = viewport.toVector2d()
		val screenPosition = (Vector2d(relativeScreenPosition.x, -relativeScreenPosition.y) / 2.0 + Vector2d(0.5)) * screenSize

		if (!clampByViewport) return screenPosition
		return screenPosition.upper(Vector2d.ZERO).lower(screenSize)

	}
}

interface Render {
	var environment:Environment
	val camera:Camera
	val root: SceneNode
	val nextTimeFuture:Future<Double>

	fun runRenderLoop()

	fun addChild(value: SceneNode) {
		return root.addChild(value)
	}

	fun removeChild(value: SceneNode) {
		return root.removeChild(value)
	}
}